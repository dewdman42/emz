# Expression Map Zoom (emz)

[[_TOC_]]

Tool for expanding _Cubase_ ExpressionMaps to include all possible slot group combinations using the slots provided.  All keyswitches will be combined in each of the slot combinations.  This is a javascript command-line tool for now.  However it is possible to setup a key command in Finder to avoid having to use the Terminal (OSX).


## Installation

Download the pre-built binary executable from Release section [HERE](https://gitlab.com/dewdman42/emz/-/releases).  (OSX and Windows).  These are command line binaries, so save them in your `PATH`.  The typically good place to place this binary on MacOS is in `/usr/local/bin`.  I am not familiar with Windows for the best place, I leave that to windows users to figure out.

## Terminal Usage

Use terminal to issue the following command line:

```
emz [options] file
```
  
By default the result will be saved in a file with a special suffix (EMZ) added to the file name.  For example:

```
emz myfile.expressionmap
```

The above will create the following output file:

```
myfile.EMZ.expressionmap
```
You can get more help about options which can be used to change the output behavior a bit with the following:

```
emz -h
emz --help
```
    
Then open the new expressionmap in _Cubase_ and enjoy!


## Desktop Integration

It is definitely possible to integrate this into the OSX Finder by using Automator to create an Action which can be applied to selected files with a key command.  This makes it possible to use emz without ever opening the the terminal other than to install it.  See the `Automator` subdir of this project for more information about using this.  https://gitlab.com/dewdman42/emz/-/tree/master/automator


## Configuring the Expression Map

The Cubase ExpressionMap editor is used to create a small skeleton map with the bare bones information needed.  The expression map should adhere to the following rules.  The following will use the word "Articulation" as it is used in the Cubase ExpressionMap editor.  Each Articulation will usually equate to an available keyswitch for the instrument being mapped.

1.  One Sound Slot row per "Articulation".  Each Sound Slot should have one Articulation in one of the 4 group columns.

2.  Do not repeat any Articulations as duplicates on multiple Sound Slot rows.

3.  Each Sound Slot row will have the keyswitches assigned that correspond to the Articulation represented on that Sound Slot row.

4.  Do not use ExpressionMap notation symbols at this point (you can change to that later after you generate the final expression map).  For now, all articulations must be text names.

5.  One way to organize instrument keyswitches into "groups" is to think about groups of inst features, controlled by keyswitches that are mutually exclusive.  Any kind of grouping you wish can be done.

6.  Save the expression map template to the location of your choice using the Cubase ExpressionMap editor.  That file is now ready to be expanded by the emz utility.

### Example

Here is an example input expressionmap template.

| __Remote__ | __Name__ | __Art 1__ | __Art 2__ | __Art 3__ | __Art 4__ | __Keyswitch__ |
|:----------:|:--------:|:---------:|:---------:|:---------:|:---------:|:-------------:|
|      -     | slot 1   |     A     |           |           |           |      C-1      |
|      -     | slot 2   |     B     |           |           |           |      D-1      |
|      -     | slot 3   |           |     C     |           |           |      E-1      |
|      -     | slot 4   |           |     D     |           |           |      F-1      |


Taking the above expressionmap as input, emz will output the following expressionmap using all possible group combinations:

| __Remote__ | __Name__ | __Art 1__ | __Art 2__ | __Art 3__ | __Art 4__ | __Keyswitch__ |
|:----------:|:--------:|:---------:|:---------:|:---------:|:---------:|:-------------:|
|      -     | slot 1   |     A     |           |           |           |      C-1      |
|      -     | slot 2   |     B     |           |           |           |      D-1      |
|      -     | slot 3   |           |     C     |           |           |      E-1      |
|      -     | slot 4   |           |     D     |           |           |      F-1      |
|      -     | slot 5   |     A     |     C     |           |           | C-1 + E-1     |
|      -     | slot 6   |     A     |     D     |           |           | C-1 + F-1     |
|            | slot 7   |     B     |     C     |           |           | C-1 + E-1     |
|            | slot 8   |     B     |     D     |           |           | C-1 * F-1     |

That may not seem like much at first, but if you add more group columns and more articulations in the form of more rows, the possible combinations start to add up to many resulting sound slot row

As explained above, the keyswitches are combined in each of the combination rows automatically


## Defaults

emz has a feature that allows you to specify default keyswitches for any given group column, whereby the final expression map will assume that any sound slots with that column empty, will have those default keyswitches added.  This is really useful, for example, if you want to have a single expressionmap lane for "mutes", and you don't want to have to include an "un-mute" lane in order to send the appropriate keyswitch to turn mutes back off.  Just one example.  

In order to use this feature, create a sound slot row that has an articulation name surrounded by curly braces.  It doesn't matter what word is used inside the curly braces, but it must be unique.  For example:  "__{default}__".  Assign the default keyswitches to this row.  emz will recognize this special virtual articulation and insert the default keyswitches on the rows where that group column is empty.  The __{default}__ sound slot will not be included in the final output.

### Example

For example, the following input expressionmap template builds upon the previous example with a third group column to use for enabling mutes.


| __Remote__ | __Name__ | __Art 1__ | __Art 2__ | __Art 3__ | __Art 4__ | __Keyswitch__ |
|:----------:|:--------:|:---------:|:---------:|:---------:|:---------:|:-------------:|
|      -     | slot 1   |     A     |           |           |           |      C-1      |
|      -     | slot 2   |     B     |           |           |           |      D-1      |
|      -     | slot 3   |           |     C     |           |           |      E-1      |
|      -     | slot 4   |           |     D     |           |           |      F-1      |
|      -     | slot 5   |           |           |   Mute    |           |      G-1      |
|      -     | slot 6   |           |           |  {UnMute}   |           |      A-1      |


This would result in the following emz output:

| __Remote__ | __Name__ | __Art 1__ | __Art 2__ | __Art 3__ | __Art 4__ | __Keyswitch__ |
|:----------:|:--------:|:---------:|:---------:|:---------:|:---------:|:-------------:|
|      -     | slot 1   |     A     |           |           |           |  C-1 + A-1    |
|      -     | slot 2   |     B     |           |           |           |  D-1 + A-1    |
|      -     | slot 3   |           |     C     |           |           |  E-1 + A-1    |
|      -     | slot 4   |           |     D     |           |           |  F-1 + A-1    |
|      -     | slot 5   |           |           |   Mute    |           |      G-1      |
|      -     | slot 5   |     A     |     C     |           |           |C-1 + E-1 + A-1|
|      -     | slot 6   |     A     |     D     |           |           |C-1 + F-1 + A-1|
|            | slot 7   |     B     |     C     |           |           |C-1 + E-1 + A-1|
|            | slot 8   |     B     |     D     |           |           |C-1 * F-1 + A-1|
|      -     | slot 9   |     A     |           |   Mute    |           |  C-1 + G-1    |
|      -     | slot 10  |     B     |           |   Mute    |           |  D-1 + G-1    |
|      -     | slot 11  |           |     C     |   Mute    |           |  E-1 + G-1    |
|      -     | slot 12  |           |     D     |   Mute    |           |  F-1 + G-1    |
|      -     | slot 13  |     A     |     C     |   Mute    |           |C-1 + E-1 + G-1|
|      -     | slot 14  |     A     |     D     |   Mute    |           |C-1 + F-1 + G-1|
|            | slot 15  |     B     |     C     |   Mute    |           |C-1 + E-1 + G-1|
|            | slot 16  |     B     |     D     |   Mute    |           |C-1 * F-1 + G-1|



In the above, the keyswitches defined on slot 6 of the input expression map will be included in all non-mute slots, so this is a way to configure the un-mute keyswitch in one place to use for those.  Meanwhile the actual pianoroll editor will not need an un-mute lane, just a single Mute lane.  _Note, the above only works if Mute is an ATTRIBUTE style articulation.  If you want to use DIRECTION style, then you will need an unmute lane unfortunately._

