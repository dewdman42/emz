C/C++ version of emz, based on JUCE xml parsing.  

For now using JUCE, mainly for its XML parsing.  There are other free
alternatives for XML so may switch to those because of JUCE licensing
but for now using this as opporunity to star to learn JUCE.

# Building

To build this project, you will need to download and install JUCE 6.x, open the `emz.jucer` file and then export it and build it in Xcode or VisualStudio

# Future

In the future may try to use CMake in order to make it easier to build this,
but I plan to provide pre-built binaries for OSX/Windows anyway.
