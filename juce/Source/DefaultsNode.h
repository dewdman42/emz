/*
  ==============================================================================

    DefaultsNode.h

  ==============================================================================
*/

#pragma once
#include <vector>
#include <JuceHeader.h>

class DefaultsNode {
public:
    DefaultsNode();
    
    // member variables
    juce::XmlElement* slot;  // <obj>
    juce::StringArray artList;
};
