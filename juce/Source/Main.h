/*
  ==============================================================================

    Main.h

  ==============================================================================
*/

#pragma once
#include <vector>
#include <JuceHeader.h>

// function declarations

void thinSlotVisuals( juce::XmlElement* svs );
void buildMatrix();
void buildMatrixSlot( juce::XmlElement * slot );
juce::XmlElement* firstObj( juce::XmlElement * list );
juce::XmlElement* nextObj( juce::XmlElement * obj );
void Combo_x2(int g1, int g2);
void Combo_x3(int g1, int g2, int g3);
void Combo_x4(int g1, int g2, int g3, int g4);
bool validateCombo(std::vector<int>& set);
void createSlot(std::vector<int>& groups, std::vector<int>& items);
void fillDefaults();
bool isDefaultNeeded( juce::XmlElement* sv, int grp);
void addDefault( juce::XmlElement* slot, int grp);
juce::XmlElement* getSvFromSlot(juce::XmlElement* slot);
void usage(char* cmd);
