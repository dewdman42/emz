/*
  ==============================================================================

    MatrixNode.h

  ==============================================================================
*/

#pragma once

#include <vector>
#include <JuceHeader.h>

class MatrixNode {
public:
    MatrixNode();
    
    // members
    juce::String name;
    int   color;
    juce::XmlElement* svList;   // <list>
    juce::XmlElement* thru;     // <obj>
    juce::XmlElement* mAction;  // <obj>
};

