/*
  ==============================================================================

  emz  (Expression Map Zoom)
  
  v 0.03 (beta)
  
  This application will take an input ExpressionMap file from Cubase, matching
  certain requirements and zoom it out to all the sound slots needed
  
  More info found on GitLab: https://gitlab.com/dewdman42/emz
 
  ==============================================================================
*/

#define VERSION "0.03 (beta)"

#include <JuceHeader.h>
#include <vector>
#include "Main.h"
#include "DefaultsNode.h"
#include "MatrixNode.h"

//=========================
// Globals
//=========================

// Is there a better way to use vector of non-pointers?
// This might be causing memory leaks, not that we care since
// shortlived cmd line tool.  If we care, then need to use better OOP
// Mainly the global vectors have memory leaks, but possible some XML too

// TODO use a proper juce::ConsoleApplication instead of the simple main()
// this way we can use the addCOmmand, etc..and also catch the failures when
// I check file arguments, etc.  Hard to find info about this.


typedef std::vector<MatrixNode*> matrix_vector_t;
typedef std::vector<DefaultsNode*> defaults_vector_t;

matrix_vector_t     gMatrix[4];   // 4 matrix vectors
defaults_vector_t   gDefaults[4]; // 4 default vectors

juce::XmlElement*     gInSlotList = nullptr;
juce::XmlElement*     gOutSlotList = nullptr;
juce::XmlElement*     gOutputXml = nullptr;

int idCounter = 10001;

void usage() {
    std::cerr << "Usage: emz [options] <file>" << std::endl << std::endl;
    
    std::cerr << "Options:" << std::endl;
    std::cerr << "  -v, --version                 output the version number" << std::endl;
    std::cerr << "  -o <file>, --outfile=<file>   output the version number" << std::endl;
    std::cerr << "  -x <txt, --suffix=<txt>       output file suffix (default: \"EMZ\"" << std::endl;
    std::cerr << "  -s, --stdout                  output to stdout" << std::endl;
    std::cerr << "  -h, --help                    display help" << std::endl << std::endl;
    
    std::cerr << "Without any options, the default will be to generate a file" << std::endl;
    std::cerr << "using the same filename and with the default suffix added" << std::endl << std::endl;
}

//===============================================
//  __  __       _
// |  \/  | __ _(_)_ __
// | |\/| |/ _` | | '_ \
// | |  | | (_| | | | | |
// |_|  |_|\__,_|_|_| |_|
//
// Parse command line arguments, open a single expressionmap xml file
// Create an output Xml tree and copy over various things from the
// input so that it will beasically end up the expanded version
// Then convert back to XmlDoc and output as XML
//===============================================

int main (int argc, char* argv[])
{

    auto args = juce::ArgumentList (argc, argv);
    
    if(args.size() < 1) {
        usage();
        exit(1);
    }
    
    if( args.containsOption("-h|--help")) {
        usage();
        exit(0);
    }
    
    if(args.containsOption("-v|--version")) {
        std::cerr << "emz v" << VERSION << std::endl;
        exit(0);
    }
    
    juce::String fileSuffix = "EMZ";
    if(args.containsOption("-x|--suffix")) {
        fileSuffix = args.removeValueForOption("-x|--suffix");
        args.removeOptionIfFound("-x|--suffix");
    }

    juce::File oFile;
    bool fileSpec = false;
    bool useStdout = false;
    
    if(args.containsOption("-o|--outfile")) {
        fileSpec = true;
        useStdout = false;
        oFile = args.getFileForOptionAndRemove("-o|--outfile");
        args.removeOptionIfFound("-o|--outfile");
   }
    
    // we may not need this if its the default behavior
    if(args.containsOption("-s|--stdout")) {
        fileSpec = false;
        useStdout = true;
        args.removeOptionIfFound("-s|--stdout");
   }

    // Any remaining arguments are input files

    juce::File inFile;
    
    if(args.size() < 1) {
        std::cerr << "ERROR: Missing input filename" << std::endl << std::endl;
        usage();
        exit(1);
    }
    
    // TODO, improve to support multiple input files in one call?
    else {
        
        inFile = args.arguments[0].text;
        
        if( ! inFile.existsAsFile() ) {
            std::cerr << "ERROR - file not found: " << args.arguments[0].text << std::endl;
            exit(1);
        }
    }
    
    // default behavior file.EMZ.ext
    
    if(!fileSpec && !useStdout) {
        oFile = inFile.getFullPathName();
        auto ext = oFile.getFileExtension();
        ext = "." + fileSuffix + ext;
        oFile = oFile.withFileExtension(ext);
    }
    
    // Create XMLDoc from that File
    juce::XmlDocument inputDoc(inFile);
    
    // get smart get pointer to first element
    std::unique_ptr<juce::XmlElement> xml =
                 inputDoc.getDocumentElementIfTagMatches("InstrumentMap");
    if(!xml) {
        std::cerr << "ERROR: Invalid ExpressionMap XML," <<
                     "missing <InstrumentMap>" << std::endl;
        exit(1);
    }
    
    //==================================
    // Initialize Output XML
    //==================================
    
    // Create output XML, copy over a couple known things
    juce::XmlElement outputXml("InstrumentMap");
    gOutputXml = &outputXml;
    
    // <string name="name">
    auto node = xml->getChildByAttribute("name","name");
    outputXml.addChildElement(new juce::XmlElement(*node));
    
    // Get slotvisuals, thin and copy
    auto slotvisuals = xml->getChildByAttribute("name", "slotvisuals");
    if(slotvisuals==nullptr) {
        std::cerr << "ERROR: Invalid ExpressionMap XML, " <<
        "missing <member name=\"slotvisuals\">" << std::endl;
        exit(1);
    }
    thinSlotVisuals(slotvisuals);
    outputXml.addChildElement(new juce::XmlElement(*slotvisuals));

    // setup output SoundSlotList
    auto slots = outputXml.createNewChildElement("member");
    slots->setAttribute("name", "slots");
    node = slots->createNewChildElement("int");
    node->setAttribute("name", "ownership");
    node->setAttribute("value", "1");
    gOutSlotList = slots->createNewChildElement("list");
    gOutSlotList->setAttribute("name", "obj");
    gOutSlotList->setAttribute("type", "obj");
    
    // <member name="controller">
    node = xml->getChildByAttribute("name", "controller");
    outputXml.addChildElement(new juce::XmlElement(*node));

    //===================================
    // assign input soundSlotList to global
    //===================================
    
    // sound slot list
    slots = xml->getChildByAttribute("name", "slots");
    if(slots==nullptr) {
        std::cerr << "ERROR: Invalid ExpressionMap XML, " <<
        "missing <member name=\"slots\">" << std::endl;
        exit(1);
    }
    
    gInSlotList = slots->getChildByName("list");
    if(gInSlotList==nullptr) {
        std::cerr << "ERROR: Invalid ExpressionMap XML, " <<
        "missing any Sound Slots" << std::endl;
        exit(1);
    }

    //================================================
    // build matrix data from input sound slots
    //================================================
    
    buildMatrix();

    //===================================
    // Generate all Sound Slot combos
    //===================================
    
    //---------------
    Combo_x2(0,1);
    Combo_x2(0,2);
    Combo_x2(0,3);
    Combo_x3(0,1,2);
    Combo_x3(0,1,3);
    Combo_x3(0,2,3);
    Combo_x4(0,1,2,3);
    Combo_x2(1,2);
    Combo_x2(1,3);
    Combo_x3(1,2,3);
    Combo_x2(2,3);
    //---------------

    //=============================
    // convert default markers
    //=============================
    
    fillDefaults();
    
    //=============================
    // Write Output XML
    // TODO, how to write to stdout?
    // juce doesn't seem to support
    // directly
    //=============================
    
    if(useStdout) {
        std::cout << outputXml.toString();
    }
    else {
        outputXml.writeTo(oFile);
        std::cerr << "Successful Zoom!" << std::endl;
    }
    
   // TODO cleanup memory leaks
        return 0;
}

//============================================================
// thinSlotVisuals
// Loop through slotVisuals array and delete any {DFLT} rows
// we won't need it after we also remove from slotsArray
//
void thinSlotVisuals(juce::XmlElement* svs) {
    
    auto slotVisuals = svs->getChildByName("list");
    if(slotVisuals==nullptr) {
        std::cerr << "ERROR: Invalid ExpressionMap XML," <<
                     " missing SlotVisuals list:" <<
                     "<list name=\"obj\" type=\"obj\">" << std::endl;
        exit(1);
    }

    // Loop through all the slotVisuals list objects.
    auto child = slotVisuals->getFirstChildElement();
    while(child != nullptr) {
        
        auto next = child->getNextElementWithTagName("obj");
        
        // make sure first element is <obj>, if not skip it
        //
        if( ! child->hasTagName("obj")) {
            child = next;
            continue;
        }
        
        // Look for {DFLT} entries, which we will remove here
        auto text = child->getChildByAttribute("name", "text");
        auto value = text->getStringAttribute("value");
        
        if(value.startsWithChar('{')) {
            slotVisuals->removeChildElement(child, true);
        }
        child = next;
    }
}


//============================================================
// buildMatrix
// Copy the soundSlotList so that we can thin
// Loop through soundSlotList and build up the matrix,
// remove certain lines from the soundSlotList
//
void buildMatrix() {

    auto slot = firstObj(gInSlotList);
    if(slot==nullptr) {
        std::cerr << "ERROR: Invalid ExpressionMap XML," <<
        " missing any SoundSlots" << std::endl;
        exit(1);
    }

    while(slot != nullptr) {
        auto next = nextObj( slot );
        buildMatrixSlot( slot );
        slot = next;
    }
}

//===================================================
//  Parse one soundslot
//
void buildMatrixSlot( juce::XmlElement* slot) {
       
    auto sv = slot->getChildByAttribute("name", "sv");
    
    // if no sv entry, then it must be an empty slot, skip it
    if(sv == nullptr) {
        return;
    }
    
    auto svList = sv->getChildByName("list");
    if(svList==nullptr) {
        // Slot doesn't have any articulations, skip it.
        return;
    }

    // Check for multiple <obj> under this <list> which we don't allow
    if(svList->getNumChildElements() > 1) {
        std::cerr << "ERROR: multiple articulation in slot not allowed" << std::endl;
        exit(1);
    }
    
    // if no sv entry, then it must be an empty slot,skip it
    auto svObj = firstObj(svList);
    if(svObj==nullptr) {
        return;
    }
    
    // get color of the sound slot
    auto colorElem = slot->getChildByAttribute("name", "color");
    int color = colorElem->getIntAttribute("value");

    auto svNameElem = svObj->getChildByAttribute("name", "text");
    auto svName = svNameElem->getStringAttribute("value");
    auto svDescElem = svObj->getChildByAttribute("name", "description");
    auto svDesc = svDescElem->getStringAttribute("value");
    auto groupElem = svObj->getChildByAttribute("name", "group");
    int group = groupElem->getIntAttribute("value"); // 0-3
    
    // Check for "{" default case, save in different place,
    // once per group
    if(svName.startsWithChar('{')) {

        // Add defaults array element to this group
        defaults_vector_t& dvec = gDefaults[group];
        dvec.push_back( new DefaultsNode() );
        auto dnode = dvec[dvec.size()-1];
        
        if(svDesc.startsWithChar('[')) {
            dnode->artList.addTokens(svDesc.substring(1,
                                    svDesc.length()-2), ",");
            dnode->artList.trim();
        }
        
        // do NOT copy this one to the gOutSlotList
        
        dnode->slot = new juce::XmlElement(*slot);
    }
    
    // Else  add to the matrix and copy as is to the gOutSlotList
    else {
        matrix_vector_t& mvec = gMatrix[group];
        mvec.push_back( new MatrixNode() );
        auto mnode = mvec[mvec.size()-1];
        
        mnode->name = svName;
        mnode->color = color;
        mnode->svList = svList;
        mnode->thru = slot->getChildByAttribute("class",
                                                "PSlotThruTrigger");
        mnode->mAction = slot->getChildByAttribute("class",
                                                   "PSlotMidiAction");

        // Copy slot to the gOutSlotList, leave ID as is
        auto child = new juce::XmlElement(*slot);
        gOutSlotList->addChildElement(child);
    }
}

//=====================
// Combination Loopers
//=====================

void Combo_x2(int g1, int g2) {

    std::vector<int> set = {g1, g2};
    int i1, i2 = 0;
    matrix_vector_t& gvec1 = gMatrix[g1];
    matrix_vector_t& gvec2 = gMatrix[g2];

    if( validateCombo(set) ) {
        
        for( i1=0; i1 < gvec1.size(); i1++) {
            for( i2=0; i2 < gvec2.size(); i2++) {
                std::vector<int> items = {i1, i2};
                createSlot( set, items );
            }
        }
    }
}

void Combo_x3(int g1, int g2, int g3) {
    
    std::vector<int> set = {g1, g2, g3};
    int i1, i2, i3 = 0;
    matrix_vector_t& gvec1 = gMatrix[g1];
    matrix_vector_t& gvec2 = gMatrix[g2];
    matrix_vector_t& gvec3 = gMatrix[g3];

    if(validateCombo(set)) {
        
        for( i1=0; i1 < gvec1.size(); i1++) {
            for( i2=0; i2 < gvec2.size(); i2++) {
                for( i3=0; i3 < gvec3.size(); i3++) {
                    std::vector<int> items = {i1, i2, i3};
                    createSlot( set, items );
                }
            }
        }
    }
}

void Combo_x4(int g1, int g2, int g3, int g4) {

    std::vector<int> set = {g1, g2, g3, g4};
    int i1, i2, i3, i4 = 0;
    matrix_vector_t& gvec1 = gMatrix[g1];
    matrix_vector_t& gvec2 = gMatrix[g2];
    matrix_vector_t& gvec3 = gMatrix[g3];
    matrix_vector_t& gvec4 = gMatrix[g4];
    
    if(validateCombo(set)) {
        
        for( i1=0; i1 < gvec1.size(); i1++) {
            for( i2=0; i2 < gvec2.size(); i2++) {
                for( i3=0; i3 < gvec3.size(); i3++) {
                    for( i4=0; i4 < gvec4.size(); i4++) {
                        std::vector<int> items = {i1, i2, i3, i4};
                        createSlot( set, items );
                    }
                }
            }
        }
    }
}

//=========================================
// validateCombo()
//
bool validateCombo(std::vector<int>& set) {
    
    int i,g = 0;
    
    for( i=0; i < set.size(); i++) {
          g = set[i];
         if(gMatrix[g].size() < 1) {
             return false;
         }
     }
     return true;
}

//==========================================
// createSlot()
//
void createSlot(std::vector<int>& groups, std::vector<int>& items) {
    
    int g, grp, first, mgrp, i, item, color = 0;

    std::vector<juce::XmlElement*> svList(4);
    std::vector<juce::XmlElement*> mAction(4);
    
    /* get first group and first item */
    grp = groups[0];
    first = groups[0];
    item = items[0];

    /***********************************************
     * get stuff from first group of this combo
     * including name, color and noteChangers
     ***********************************************/
    
    auto mnode = gMatrix[grp][item];
    
    juce::String slotName(mnode->name);
    color = mnode->color;
    svList[grp] = mnode->svList;
    mAction[grp] = mnode->mAction;

    /************************************************
     * Loop through remaining groups of combo building
     * up the various data need to create slot
     ************************************************/

    for( g=1; g < groups.size(); g++) {
        grp = groups[g];
        item = items[g];

        mnode = gMatrix[grp][item];
        
        slotName += " ";
        slotName += mnode->name;
        svList[grp] = mnode->svList;
        mAction[grp] = mnode->mAction;
    }

    //=============================================
    // Create the Slot
    //==============================================
    
    auto sObj = gOutSlotList->createNewChildElement("obj");
    sObj->setAttribute("class", "PSoundSlot");
    sObj->setAttribute("ID", idCounter++);

    auto thru = sObj->createNewChildElement("obj");
    thru->setAttribute("class", "PSlotThruTrigger");
    thru->setAttribute("name", "remote");
    thru->setAttribute("ID", idCounter++);
    auto node = thru->createNewChildElement("int");
    node->setAttribute("name", "status");
    node->setAttribute("value", 144);
    node = thru->createNewChildElement("int");
    node->setAttribute("name", "data1");
    node->setAttribute("value", -1);
    
    // Build the midiout combination from the 4 sv's passed in
    // new copy here so we can build it up specially for each slot`

    auto action = sObj->createNewChildElement("obj");
    action->setAttribute("class", "PSlotMidiAction");
    action->setAttribute("name", "action");
    action->setAttribute("ID", idCounter++);
    node = action->createNewChildElement("int");
    node->setAttribute("name", "version");
    node->setAttribute("value", 600);
    
    // copy <member> noteChanger
    auto mact = mAction[first];
    auto member = mact->getChildByAttribute("name", "noteChanger");
    auto child = new juce::XmlElement(*member);
    action->addChildElement(child);
    
    // add the MidiMessages member, do this manually in order to build
    // up each collection of midi events properly
    member = action->createNewChildElement("member");
    member->setAttribute("name", "midiMessages");
    node = member->createNewChildElement("int");
    node->setAttribute("name", "ownership");
    node->setAttribute("value", "1");
    auto olist = member->createNewChildElement("list");
    olist->setAttribute("name", "obj");
    olist->setAttribute("type", "obj");

    // fill out obj array inside list
    juce::XmlElement* msg = nullptr;
    
    for(g=0; g < mAction.size(); g++) {
        mact = mAction[g];
        if(mact==nullptr) {
            continue;
        }
        msg = mact->getChildByAttribute("name", "midiMessages");
        if(msg == nullptr) {
            continue;
        }
        
        node = msg->getChildByName("list");
        if( node == nullptr) {
            continue;
        }
        
        // copy all the obj elements from src to dest
        auto obj = firstObj(node);
        while( obj != nullptr) {
            child = new juce::XmlElement(*obj);
            child->setAttribute("ID", idCounter++);
            olist->addChildElement(child);
            obj = nextObj(obj);
        }
    }

    //======================================================
    // set up member array and stash the Slotvisuals for it
    //======================================================
    
    member = sObj->createNewChildElement("member");
    member->setAttribute("name", "sv");
    node = member->createNewChildElement("int");
    node->setAttribute("name", "ownership");
    node->setAttribute("value", "2");
    olist = member->createNewChildElement("list");
    olist->setAttribute("name", "obj");
    olist->setAttribute("type", "obj");

    for( i=0; i<groups.size(); i++) {
        mgrp = groups[i];
        auto list = svList[mgrp];
        
        auto obj = firstObj(list);
        while(obj != nullptr) {
            child = new juce::XmlElement(*obj);
            child->setAttribute("ID", idCounter++);
            olist->addChildElement(child);
            obj = nextObj(obj);
        }
    }
    
    // Set Slot Name
    member = sObj->createNewChildElement("member");
    member->setAttribute("name", "name");
    node = member->createNewChildElement("string");
    node->setAttribute("name", "s");
    node->setAttribute("value", slotName);
    node->setAttribute("wide", "true");

    // Slot Color - use color of the item of the first group of this combo
    member = sObj->createNewChildElement("int");
    member->setAttribute("name", "color");
    member->setAttribute("value", color);
}


/*******************************************
 * Scan through entire slotArray and
 * look for opportunities to insert defaults
 *******************************************/

void fillDefaults() {

    // Loop through each XmlElement of the gOutSlotList,
    // for each one copy defaults
    int grp = 0;
    
    auto slot = firstObj(gOutSlotList);
    
    while(slot != nullptr) {
        
        auto svList = getSvFromSlot(slot);
        if( svList != nullptr ) {
            
            for(grp=0; grp<4; grp++) {
                if(isDefaultNeeded(svList, grp)) {
                    addDefault(slot, grp);
                }
            }
        }
        
        slot = nextObj(slot);
    }
}

juce::XmlElement* getSvFromSlot(juce::XmlElement* slot) {
    
    auto sv = slot->getChildByAttribute("name", "sv");
    if(sv == nullptr) {
        return sv;
    }
    
    auto list = sv->getChildByName("list");
    if(list == nullptr) {
        return list;
    }

    auto obj = list->getChildByName("obj");
    if(obj == nullptr) {
        return nullptr;
    }
    
    return list;
}


/************************************
 * look through given sv array and
 * check to see if group is present
 ************************************/

bool isDefaultNeeded(juce::XmlElement* svList, int grp) {

    int i, j = 0;
    
    /*********************************************
     * first check to see if the desired obj array
     * has an entry already for this group.  If so
     * then just get out
     *********************************************/

    auto obj = firstObj(svList);
    while(obj != nullptr) {
        auto sgrpElem = obj->getChildByAttribute("name", "group");
        int sgrp = sgrpElem->getIntAttribute("value");
        if(sgrp == grp) {
            return false;
        }
        obj = nextObj(obj);
    }
    
    obj = firstObj(svList);  // TODO temporary hack?
                             // where hshould this come from?
    
    auto textElem = obj->getChildByAttribute("name", "text");
    const juce::String svName = textElem->getStringAttribute("value");

    /********************************************
     * a matching group was not found, so determine
     * whether a default should be applied
     * 2D defaults array, could be more then one
     * {DFLT} per group
     ********************************************/
    
    defaults_vector_t& dvec = gDefaults[grp]; // get vector for this group
    
    for( i=0; i< dvec.size(); i++) {

        auto dnode = dvec[i];
        
        if(dnode->artList.size() < 1 ) {
            /* no specific articulations were named, so use it */
            return true;
        }

        for( j=0; j<dnode->artList.size(); j++) {
            if(dnode->artList[j].compareIgnoreCase(svName)) {
                return true;
            }
        }
    }

    /* None of the conditions matched so don't default it */
    return false;
}


void addDefault(juce::XmlElement* slot, int grp) {
   
    int i=0;
    
    // There could be more then one defaults entry for this group
    // so loop through them all

    defaults_vector_t& dvec = gDefaults[grp];
    
    for( i=0; i<dvec.size(); i++) {

        auto dnode = dvec[i];

        auto srcAction = dnode->slot->getChildByAttribute("class",
                                                          "PSlotMidiAction");
        if(srcAction == nullptr) return;
        
        auto srcMsg = srcAction->getChildByAttribute("name",
                                                     "midiMessages");
        if(srcMsg==nullptr) {
            return;
        }
        auto list = srcMsg->getChildByName("list");
        if(list==nullptr) {
            return;
        }
        
        // TODO, what if more then one?
        auto obj = list->getChildByName("obj");
        if(obj==nullptr) {
            return;
        }

        auto srcMidi = obj;

        /***************************************************************
         * get handle to dest midiMessages array, if not there, create
         *************************************************************/
        
        auto destAction = slot->getChildByAttribute("class",
                                                    "PSlotMidiAction");
        
        auto destMsg = destAction->getChildByAttribute("name",
                                                       "midiMessages");
        if (destMsg == nullptr) {
            destMsg = destAction->createNewChildElement("member");
            destMsg->setAttribute("name", "midiMessages");
            auto child = destMsg->createNewChildElement("int");
            child->setAttribute("name", "ownership");
            child->setAttribute("value", "1");
        }
        
        auto dlist = destMsg->getChildByName("list");
        if(dlist==nullptr) {
            dlist = destMsg->createNewChildElement("list");
            dlist->setAttribute("name", "obj");
            dlist->setAttribute("type", "obj");
        }

        /******************************************************
         * Loop through all midi events on the srcMidi queue
         * and copy them to the destMidi queue
         ******************************************************/

        while(srcMidi != nullptr) {
            auto child = new juce::XmlElement(*srcMidi);
            child->setAttribute("ID", idCounter++);
            dlist->addChildElement(child);
            srcMidi = list->getNextElementWithTagName("obj");
        }
    }
}

//==========================
// Helpers
//==========================

//===================================================
// Look for first <obj> child under <list>
//
juce::XmlElement* firstObj(juce::XmlElement* list) {
    
    auto first = list->getFirstChildElement();
    
    if(!first->hasTagName("obj")) {
        first = first->getNextElementWithTagName("obj");
    }
    
    if(first==nullptr) {
        std::cerr << "ERROR: Invalid ExpressionMap XML, " <<
        "missing <obj> in <list>" << std::endl;
        exit(1);
    }
    
    else return first;
}

//===================================================
// Look for next peer <obj> linked to this one
//
juce::XmlElement* nextObj( juce::XmlElement* obj ) {
    return obj->getNextElementWithTagName("obj");
}
