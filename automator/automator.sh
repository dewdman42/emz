# 
# automator.sh
#
# This bash script should be used from inside automator action
# to call emz and report errors via Applescript
#


# needs two vars $ERRMSG and $ERRDESC
function AlertOut() {
    if [ ${#ERRMSG} -lt 1 ]
    then
        ERRMSG="ERROR processing expressionmap file"
    fi

    osascript -e "tell app \"System Events\" to beep"
    osascript -e "tell app \"System Events\" to display dialog \"${ERRMSG}\n\n${ERRDESC}\" with icon caution"
    exit 0;
}

while [ "$#" -gt 0 ]
do
  #====================
  # check file exists
  #====================
  if [ ! -f "$1" ]
  then
     ERRMSG="ERROR: File not found"
     ERRDESC="\n\n[$1]"
     AlertOut
  fi
      
  #======================
  # call emz
  #======================
  ERRDESC=`/usr/local/bin/emz "$1"`

  #=========================
  # check for error returned
  #=========================
  if [ $? -ne 0 ]
  then
     #===================================================
     # Display error message including applescript dialog
     #===================================================
     if [ ${#ERRDESC} -lt 1 ]
     then
         ERRDESC="Make sure its a valid expressionmap file"
     fi
     ERRDESC="${ERRDESC}\n\n[$1]"

     AlertOut
  fi

  shift 1	

done


# Success
#TODO, play same sound used by finder when duplicatinbg files

