# OSX Desktop Integration with Automator

In order to integrate __emz__ into the OSX Finder desktop, this __Automator__ workflow can be used:

1.  Unzip the Automator service found here (`ExprMapZoom.zip`) and save in the following folder or import into Automator:

```
   ~/Library/Services
```

2.  Once the Automator workflow has been imported, then use __OSX System Preferences__ to create a key command for this Quick Action.

3.  After doing that you can select an expressionmap file in Finder and hit that key command to instantly generate an emz-expanded expressionmap without ever having to open Terminal.  You can also select multiple expressionmap files, they will all be expanded.

_The automator.sh script does not need to be used seperately, this is here only in order to have versioning of the bash script used inside the automator workflow, but this scrdipt has already been copy and pasted into the automator workflow._


