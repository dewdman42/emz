#!/usr/bin/env /usr/local/bin/node
/**************************************************************************
 * emz.js
 *
 * Expression Map Zoom
 *
 * Version 1.0.3
 *
 *************************************************************************/

var fs = require('fs');
var parser = require('xml2json');
const { program } = require('commander');
program.version('1.0.3');
const path = require('path');

/***********************************
 * globals
 ***********************************/

var matrix = new Array(4);
for(let i=0;i<matrix.length;i++) {
    matrix[i] = [];
}
var defaults = new Array(4);
for(let i=0;i<defaults.length;i++) {
    defaults[i] = [];  // init empty array of defaults for each group
}

/* Generate unique ID values based on Date.now() for starting number */
var idCounter = Date.now();
var slotsArray;


/*****************************************************
 * Commander args
 *****************************************************/

program
  .usage("[options] <file>")
  .arguments("[file]")
  .action(function(file) {
              FILENAME=file;
          })
  .option('-o, --outfile <outfile>', 'output filename')
  .option('-s, --stdout', 'Use STDOUT instead of output file', false)
  .option('-x, --suffix <txt>', 'output file suffix','EMZ');

program.on('--help', () => {
  console.log('');
  console.log('Without any options, the default will be to generate a file');
  console.log('using the same filename and with the default suffix added');
  console.log('');
});


// Get args
program.parse(process.argv);
 
if(FILENAME == undefined) {
    console.log('');
    console.log("ERROR: missing input file name");
    console.log('');

    program.help();
    process.exit(1);
}

// Generate the output file name here

if(program.opts().stdout == false) {

    // outfile name provided, so just use it as is
    if(program.opts().outfile != undefined) {
        OUTFILE = program.opts().outfile;
    }

    // not stdout and no outfile name provided, so generate
    // a name using the suffix.
    else {
        let dir = path.dirname(FILENAME);
        let base = path.basename(FILENAME, ".expressionmap");

        base = base + "." + program.opts().suffix + ".expressionmap";

        OUTFILE = path.join(dir, base);

        // TODO, check to see if file already exists and if so, increment
        //       number until we find a unique name that can be used.
        // TODO  or should we allow to force the name without increment?
        // 
        // probably should have an option that does not overwrite
        // unless -f flag is provided, but think it through a bit.
    }
}


/************************************************************
 * read file from the filesystem and process with 
 * embedded function. 
 ************************************************************/

fs.readFile( FILENAME, function(err, data) {

    if(data == undefined) {
        console.log("ERROR: File not found:  " + FILENAME);
        process.exit(1);
    }
    var json = parser.toJson(data, {object:true, reversible:true});

    var map = json.InstrumentMap;
    var slots = memberByName(map, "slots");
    slotsArray = slots.list.obj; //global

    thinSlotVisuals(map);

    buildMatrix();

    //---------------
    Combo_x2(0,1);
    Combo_x2(0,2);
    Combo_x2(0,3);
    Combo_x3(0,1,2);
    Combo_x3(0,1,3);
    Combo_x3(0,2,3);
    Combo_x4(0,1,2,3);
    Combo_x2(1,2);
    Combo_x2(1,3);
    Combo_x3(1,2,3);
    Combo_x2(2,3);
    //---------------

    fillDefaults();

    // convert back to XML and output
    var xml = parser.toXml(JSON.stringify(json));

    //output it.
    if(program.opts().stdout) {
        console.log(xml);
    }
    else {
        fs.writeFile(OUTFILE, xml, (err) => { 
              
            // In case of a error throw err. 
            if (err) throw err; 
        });
    }

});


/****************************************************
 * thinSlotVisuals
 *
 * Look for {DFLT} lines in the slotVisuals array, and
 * just cut them out, they aren't needed
 ****************************************************/

function thinSlotVisuals(map) {

    var svs = memberByName(map, "slotvisuals");
    var slotVisuals = svs.list.obj;

    /************************************************************
     * Look through slotVisuals array and delete any {DFLT} rows.
     * we won't need it after we also remove it from slotsArray
     ************************************************************/

    for(let i=slotVisuals.length-1; i>=0; i--) {
        let sname = stringByName(slotVisuals[i], "text");
        if(sname != undefined && sname.substr(0,1) == "{") {
            slotVisuals.splice(i,1);
        }
    }
}

/****************************************************
 * buildMatrix
 *
 * iterate through the slotsArray and build up the 
 * matrix, remove certain lines from slotsArray
 ****************************************************/

function buildMatrix() {

    for(let i=0;i<slotsArray.length;i++) {

        // TODO what if only one slot?
        let sv = getSvFromSlot(slotsArray[i]);

        if(sv == undefined) {
            //  This must be an empty slot, so just ignore it and remove
            slotsArray.splice(i,1);
            i--;
            continue;
        }

        if(Array.isArray(sv)) {
           console.log("ERROR, multiple articulations in slot not allowed");
           process.exit(1);
        }

        let color = intByName(slotsArray[i], "color");

        /***********************************************
         * Look for Default SV's
         * when found, flag them somehow so that later 
         * on we can leave out and also we will need 
         * to remove from the main SLotVisuals array
         ***********************************************/

        let svName = stringByName(sv, "text");
        let svDescr = stringByName(sv, "description");
        let group = parseInt(intByName(sv, "group"));

        /***********************************************
         * if {DFLT} case, then save in different place
         * once per group.  And remove the slot row 
         * while we're here
         ***********************************************/

        if(svName.substr(0,1) == "{") {

            let dflt = {};
            dflt.slot = slotsArray[i];

            if( svDescr.substr(0,1) == "[") {
                // Look for possible comma seperated list and store
                // as an array.
                let temp = svDescr.substr(1, svDescr.length-2);
                // stash array of articulation Names
                dflt.artList = temp.split(",");
            }

            defaults[group].push(dflt); // 2d array
            slotsArray.splice(i,1);
            i--;
        }

        // else add to the matrix
        else {
            matrix[group].push({
                name: svName,
                color: color,
                sv: sv,
                thru: objElemByName( slotsArray[i], "PSlotThruTrigger"),
                trigger: objElemByName( slotsArray[i], "PSlotMidiAction")
            });
        }
    }
}

/*******************************************
 * Scan through entire slotArray and 
 * look for opportunities to insert defaults
 *******************************************/

function fillDefaults() {

    for(let i=0; i<slotsArray.length; i++) {

        let sv = getSvFromSlot(slotsArray[i]);
        if(sv != undefined) {

            // Loop through all 4 known group columns
            // for each one, look for empty situation
            for(let grp=0; grp<=3; grp++) {

                if( isDefaultNeeded(sv, grp)) {
                    addDefault(slotsArray[i], grp);        
                }
            }
        }
    }
}

function getSvFromSlot(slot) {

    let temp = memberByName(slot, "sv");
    if(temp == undefined || temp.list == undefined
            || temp.list.obj == undefined) {
        //  This must be an empty slot, so just ignore it and remove
        return undefined;
    }
    return temp.list.obj;
 }

/************************************
 * look through given sv array and
 * check to see if group is present
 ************************************/

function isDefaultNeeded(sv, grp) {

    /*********************************************
     * first check to see if the desired sv array
     * has an entry already for this group.  If so
     * then just get out
     *********************************************/

    if(Array.isArray(sv)) {
        for(let i=0; i<sv.length; i++) {
            let elem=sv[i];
            let sgrp = parseInt(intByName(sv[i], "group"));
            if(sgrp == grp) {
                return false;
            }
        }
    }
    else {
        let sgrp = parseInt(intByName(sv, "group"));
        if(sgrp == grp) {
            return false;
        }
    }

    let svName = stringByName(sv, "text");

    /********************************************
     * a matching group was not found, so determine
     * whether a default should be applied
     * 2D defaults array, could be more then one
     * {DFLT} per group
     ********************************************/
     
    let dlftList = defaults[grp];
    for(let i=0; i< dlftList.length; i++) {

        let dflt = dlftList[i];

        if(dflt.artList == undefined) {
            /* no specific articulations were named, so use it */
            return true;
        }

        for(let j=0; j<dflt.artList.length; j++) {
            if(dflt.artList[j] == svName) {
                return true;
            }
        }
    }

    /* None of the conditions matched so don't default it */
    return false;
}


function addDefault(slot, grp) {

    // first make sure there is something to copy, if not, get out
    if(slot == undefined) return;
    
    // There could be more then one defaults entry for this group
    // so loop through them all

    for(let i=0; i<defaults[grp].length; i++) {

        let dflt = defaults[grp][i];

        let srcAction = objElemByName(dflt.slot, "PSlotMidiAction");
        if(srcAction == undefined) return;
        
        let srcMsg = memberByName(srcAction, "midiMessages");
        if(srcMsg == undefined || srcMsg.list == undefined || srcMsg.list.obj == undefined) {
            return;
        }

        let srcMidi = srcMsg.list.obj;

        /***************************************************************
         * get handle to dest midiMessages array, if not there, create
         /*************************************************************/
        
        let destAction = objElemByName(slot, "PSlotMidiAction");
        let destMsg = memberByName(destAction, "midiMessages");

        if (destMsg == undefined) {
            if(destAction.member == undefined) destAction.member = [];
            destAction.member.push({
                    name:"midiMessages",
                    int:{name:"ownership", value:"1"}
            });

            destMsg = memberByName(destAction, "midiMessages");
        }
        if(destMsg.list == undefined) destMsg.list = {name:"obj", type:"obj", obj:[]}; 
        if(destMsg.list.obj == undefined) destMsg.list.obj = [];


        if( ! Array.isArray(destMsg.list.obj)) {
            let obj = destMsg.list.obj;
            destMsg.list.obj = [];
            destMsg.list.obj.push(obj);
        }
        let destMidi = destMsg.list.obj;

        /******************************************************
         * Loop through all midi events on the srcMidi queue
         * and copy them to the destMidi queue
         ******************************************************/

        if(Array.isArray(srcMidi)) {
            for(let i=0;i<srcMidi.length;i++) {
                srcMidi[i].ID = idCounter.toString();
                idCounter++;
                destMidi.push(srcMidi[i]);
            }
        }
        else {
            srcMidi.ID = idCounter.toString();
            idCounter++;
            destMidi.push(srcMidi);
        }
    }
}


/**********************************************
 * Each Combo function handles different number
 * of groups, but which exact groups are 
 * provided by args.
 **********************************************/

function Combo_x2(g1,g2) {

    if(validateCombo([g1,g2])) {

        for(let i1=0; i1<matrix[g1].length; i1++) {
            for(let i2=0; i2<matrix[g2].length; i2++) {
                prepareSlot({groups:[g1, g2],
                             items:[i1, i2]});
            }
        }
    }
}

function Combo_x3(g1,g2,g3) {

    if(validateCombo([g1,g2,g3])) {

        for(let i1=0; i1<matrix[g1].length; i1++) {
            for(let i2=0; i2<matrix[g2].length; i2++) {
                for(let i3=0; i3<matrix[g3].length; i3++) {
                    prepareSlot({groups:[g1, g2, g3],
                                 items:[i1, i2, i3]});
                }
            }
        }
    }
}

function Combo_x4(g1,g2,g3,g4) {

    if(validateCombo([g1,g2,g3,g4])) {

        for(let i1=0; i1<matrix[g1].length; i1++) {
            for(let i2=0; i2<matrix[g2].length; i2++) {
                for(let i3=0; i3<matrix[g3].length; i3++) {
                    for(let i4=0; i4<matrix[g4].length; i4++) {
                        prepareSlot({groups:[g1, g2, g3, g4],
                                     items:[i1, i2, i3, i4]});
                    }
                }
            }
        }
    }
}


/*************************************************
 * Prepare data structure for creating a lot
 * and then create the slot
 *************************************************/

function prepareSlot(args) {

    let data = {};

    var groups = args.groups;
    var items = args.items;

    /* get first group and first item */
    var grp = groups[0];
    var item = items[0];

    /***********************************************
     * get stuff from first group of this combo
     * including name, color and noteChangers
     ***********************************************/

    data.name = matrix[grp][item].name;
    data.color = matrix[grp][item].color;
    data.groups = groups;
    data.first = groups[0];
    data.sv = [];
    data.trig = [];
    
    data.sv[grp] = matrix[grp][item].sv;
    data.trig[grp] = matrix[grp][item].trigger;

    /************************************************
     * Loop through remaining groups of combo building
     * up the various data need to create slot
     ************************************************/

    for(let g=1;g<groups.length;g++) {
        grp = groups[g];
        item = items[g];

        data.name = data.name + " " + matrix[grp][item].name;
        data.sv[grp] = matrix[grp][item].sv;
        data.trig[grp] = matrix[grp][item].trigger;
    }

    /* create actual slot for this item */
    createSlot(data);     
}


/*************************************************
 * hard coded functions to generate each combo
 *************************************************/

function validateCombo(grpList) {
    for(let i=0;i<grpList.length;i++) {
        let g = grpList[i];
        if(matrix[g].length < 1) {
            return false;
        }
    }
    return true;
}


/*************************************************
 * add a new sound slot to the slotsArray, the data
 * argument contains a {} argument with numerous data
 * needed
 ***/
function createSlot(data) {

    var slot = {class:"PSoundSlot", ID: idCounter.toString()};
    idCounter++;

    // what about ID, leave blank for now

    slot.obj = [];
    slot.member = [];

    slot.obj.push({
        class:"PSlotThruTrigger", name:"remote",
        ID: idCounter.toString(),
        int: [ {name:"status", value:"144"}, {name:"data1", value:"-1"} ]
    });

    idCounter++;

    // Build the midiout combination from the 4 sv's passed in
    // new copy here so we can build it up specially for each slot`

    slot.obj.push({
        class:"PSlotMidiAction", name:"action",
        ID: idCounter.toString(),
        int:{name:"version", value:"600"},
        member:[]
    });

    idCounter++;

    slot.obj[1].member.push({
        name:"noteChanger",
        int:{name:"ownership", value:"1"},
        list:{name:"obj",
            type:"obj",
            obj:{
                class: "PSlotNoteChanger",
                ID: idCounter.toString(),
                int:[], 
                float:[]
            }
        }
    });

    idCounter++;

    var changer = memberByName(data.trig[data.first], "noteChanger");
    var changeobj = objElemByName(changer.list, "PSlotNoteChanger");


    slot.obj[1].member[0].list.obj.int.push({
                name:"channel",
                value: intByName(changeobj, "channel") });

    slot.obj[1].member[0].list.obj.int.push({
                name:"minVelocity",
                value: intByName(changeobj, "minVelocity") });

    slot.obj[1].member[0].list.obj.int.push({
                name:"maxVelocity", value:
                intByName(changeobj, "maxVelocity") });

    slot.obj[1].member[0].list.obj.int.push({
                name:"transpose", value:
                intByName(changeobj, "transpose") });

    slot.obj[1].member[0].list.obj.int.push({
                name:"minPitch", value:
                intByName(changeobj, "minPitch") });

    slot.obj[1].member[0].list.obj.int.push({
                name:"maxPitch", value:
                intByName(changeobj, "maxPitch") });

    slot.obj[1].member[0].list.obj.float.push({
                name:"velocityFact", value:
                floatByName(changeobj, "velocityFact") });

    slot.obj[1].member[0].list.obj.float.push({
                name:"lengthFact", value:
                floatByName(changeobj, "lengthFact") });


    slot.obj[1].member.push({
            name:"midiMessages",
            int:{name:"ownership",
            value:"1"},list:{name:"obj", type:"obj", obj:[]}
    });



    for(let grp=0;grp<data.trig.length;grp++) {

        let msg = memberByName(data.trig[grp], "midiMessages");
        if(msg != undefined && msg.list != undefined && msg.list.obj != undefined) {
            let queue = msg.list.obj;
            if(Array.isArray(queue)) {
                for(let i=0;i<queue.length;i++) {
                    queue[i].ID = idCounter.toString();
                    idCounter++;
                    slot.obj[1].member[1].list.obj.push(queue[i]);
                }
            }
            else {
                queue.ID = idCounter.toString();
                idCounter++;
                slot.obj[1].member[1].list.obj.push(queue);
            }
        }
    }


    //======================================================
    // set up member array and stash the Slotvisuals for it
    //======================================================

    slot.member.push({
        name:"sv",
        int:{name:"ownership", 
           value:"2"},
        list:{name:"obj", 
            type:"obj", 
            obj:[]}
    });

    for(let i=0;i<data.groups.length;i++) {
        let grp = data.groups[i];
        slot.member[0].list.obj[i] = data.sv[grp];
    }
    
    // set Slot Name
    slot.member.push({name:"name", string: {name:"s", value:data.name, wide:"true"}});

    // Slot Color - use color of the item of the first group of this combo
    slot.int = {name:"color", value:data.color};

    // Add to the slots array
    slotsArray.push(slot);
}


/**********************
 * Accessor Functions
 **********************/

function memberByName(subJson, name) {
    if(subJson == undefined || subJson.member == undefined) {
        return undefined;
    }

    if(Array.isArray(subJson.member)) {
        for(let i=0;i<subJson.member.length;i++) {
            if(subJson.member[i].name == name) {
                return subJson.member[i];
            }
        }
    }
    else {
        if(subJson.member.name == name) {
            return subJson.member;
        }
    }
    return undefined;
}

function stringByName(subJson, name) {
    if(subJson == undefined || subJson.string == undefined) {
        return undefined;
    }

    if(Array.isArray(subJson.string)) {
        for(let i=0;i<subJson.string.length;i++) {
            if(subJson.string[i].name == name) {
                return subJson.string[i].value;
            }
        }
    }
    else {
        if(subJson.string.name == name) {
            return subJson.string.value;
        }
    }

    return undefined;
}

function intByName(subJson, name) {
    if(subJson == undefined || subJson.int == undefined) {
        return undefined;
    }

    if(Array.isArray(subJson.int)) {
        for(let i=0;i<subJson.int.length;i++) {
            if(subJson.int[i].name == name) {
                return subJson.int[i].value;
            }
        }
    }
    else {
        if(subJson.int.name == name) {
            return subJson.int.value;
        }
    }
    return undefined;
}

function floatByName(subJson, name) {
    if(subJson == undefined || subJson.float == undefined) {
        return undefined;
    }

    if(Array.isArray(subJson.float)) {
        for(let i=0;i<subJson.float.length;i++) {
            if(subJson.float[i].name == name) {
                return subJson.float[i].value;
            }
        }
    }
    else {
        if(subJson.float.name == name) {
            return subJson.float.value;
        }
    }
    return undefined;
}

function objElemByName(subJson, name) {
    if(subJson == undefined || subJson.obj == undefined) {
        return undefined;
    }

    if(Array.isArray(subJson.obj)) {
        for(let i=0;i<subJson.obj.length;i++) {
            if(subJson.obj[i].class == name) {
                return subJson.obj[i];
            }
        }
    }
    else {
        if(subJson.obj.class == name) {
            return subJson.obj;
        }
    }
    return undefined;
}
